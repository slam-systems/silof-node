"use strict";

var os = require("os");

var LogLevels = require("./log_levels.js");

var Logger = function Logger(name) {
	if(!name) throw new Error("name argument is required.");
	this.name = name;
	this.sinks = [];
};
Logger.prototype.addSink = function addSink(minLevel, sink) {
	if(typeof sink !== "object" || typeof sink.log !== "function")
		throw new Error("sink must be a sink.");
	this.sinks.push([ LogLevels.parse(minLevel), sink ]);
};
Logger.prototype.close = function close() {
	this.sinks.forEach(function(s) {
		if(typeof s[1].close === "function") s[1].close();
	});
};
Logger.prototype.log = function log(level, args) {
	var l = {
		data : (arguments.length > 2) ?
			Array.prototype.slice.call(arguments, 1) :
			args,
		hostname : os.hostname(),
		level : LogLevels.parse(level),
		name : this.name,
		pid : process.pid,
		time : Math.floor(Date.now() / 1000)
	};
	this.sinks.filter(function(s) {
		return s[0] <= l.level;
	}).forEach(function(s) {
		s[1].log(l);
	});
	if(l.level >= LogLevels.FATAL) process.exit()
};

Object.keys(LogLevels).forEach(function(level) {
	Logger.prototype[level.toLowerCase()] = function(args) {
		this.log.apply(this, [level].concat(Array.prototype.slice.call(arguments)));
	};
});

module.exports = {
	ConsoleSink : require("./sink_console.js"),
	FileSink : require("./sink_file.js"),
	Logger : Logger,
	LogLevels : LogLevels
};
