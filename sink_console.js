var colors = require("colors/safe");
var LogLevels = require("./log_levels.js");
var util = require("util");

var ConsoleSink = function ConsoleSink() {};
ConsoleSink.prototype.color = function color(level) {
	if(level < LogLevels.INFO) return colors.magenta;
	else if(level < LogLevels.WARN) return colors.cyan;
	else if(level < LogLevels.ERROR) return colors.yellow;
	else if(level < LogLevels.FATAL) return colors.red;
	else return colors.red.bold.underline;
};
ConsoleSink.prototype.log = function log(l) {
	console.log(util.format("[%s] %s@%s/%d: %s",
		colors.cyan(new Date(l.time * 1000).toLocaleString()),
		colors.bold(l.name), l.hostname, l.pid,
		this.color(l.level)(util.format("%s %j", LogLevels.toString(l.level), l.data))
	));
};

module.exports = ConsoleSink;
