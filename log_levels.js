var LogLevels = {
	ALL :    0,
	TRACE : 10,
	DEBUG : 20,
	INFO :  30,
	WARN :  40,
	ERROR : 50,
	FATAL : 60
};

LogLevels.parse = function parse(level) {
	if(typeof level === "number") return level;
	else if(typeof level === "string") return LogLevels[level.toUpperCase()];
	else throw new Error("Invalid type");
};
LogLevels.toString = function toString(level) {
	for(var l in LogLevels) {
		if(LogLevels[l] === level) return l;
	}
	return level.toString();
};

module.exports = LogLevels;
