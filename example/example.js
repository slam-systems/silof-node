#!/usr/bin/env node

var silof = require("..");

logger = new silof.Logger("example");
logger.addSink("trace", new silof.ConsoleSink());
logger.addSink(silof.LogLevels.WARN, new silof.FileSink("example.log"));

logger.trace("trace", silof.LogLevels.TRACE);
logger.debug("debug", silof.LogLevels.DEBUG);
logger.info("info", silof.LogLevels.INFO);
logger.warn("warn", silof.LogLevels.WARN);
logger.error("error", silof.LogLevels.ERROR);
logger.fatal("fatal", silof.LogLevels.FATAL);

logger.log(silof.LogLevels.INFO, "Hello, world!");

logger.close();
