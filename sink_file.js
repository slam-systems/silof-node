var fs = require("fs");

var FileSink = function FileSink(filename) {
	this.f = fs.createWriteStream(filename, {
		flags : "a",
	});
};
FileSink.prototype.close = function close() {
	this.f.end();
};
FileSink.prototype.log = function log(l) {
	this.f.write(JSON.stringify(l) + "\n", "utf8");
};

module.exports = FileSink;
